# Powtorka

A REST API for revising learned words and notes using a spaced repetition method.

## Technology stack & libraries

#### Data

- Flyway migrations
- PostgreSQL

#### Backend

- Java 11
- Spring Boot 2.3.5
- Spring Data JPA 
- Spring Security: JWT & password encoding

#### Libraries

- Lombok
- Jackson
- Swagger: springfox-swagger 2 & springfox-swagger-ui
- [spring-dotenv 2.3.0 (storing configs in environment)](https://github.com/paulschwarz/spring-dotenv)


## Features

### Authentication

- JWT-based Authentication: log in & log out
- Sign up: username is unique

### Cards

A card is a central entity: it contains information to be memorized. Card's title is unique.

- A user can get his own card by id
- A user can create a card

### Other things

- Custom DTO validation
- Exception handling with controller advice
- Swagger documentation (`{base-url}/swagger-ui.html`)

---

## Resources

Tutorials:

- [Spring Boot Exception Handling Tutorial](https://www.toptal.com/java/spring-boot-rest-api-error-handling)
- [Set JWT with Spring Boot and Swagger UI](https://www.baeldung.com/spring-boot-swagger-jwt)
- [How to configure Swagger in Spring Boot - Brain Bytes](https://www.youtube.com/watch?v=8s9I1G4tXhA)
- [Documenting Swagger](https://www.vojtechruzicka.com/documenting-spring-boot-rest-api-swagger-springfox)

Reference projects:

- [Spring 5 RESTful API Example](https://github.com/mkdika/spring5-rest-api)