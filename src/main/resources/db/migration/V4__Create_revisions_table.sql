CREATE TABLE revisions (
	revision_id BIGSERIAL PRIMARY KEY,
	user_id BIGINT NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	card_id BIGINT NOT NULL UNIQUE,
	times_revised INTEGER NOT NULL DEFAULT 0 CHECK (times_revised >= 0),
	active BOOLEAN DEFAULT TRUE,
	next_revision TIMESTAMP,
	last_revision TIMESTAMP,
	CONSTRAINT fk_user
		FOREIGN KEY(user_id)
			REFERENCES users(user_id)
			ON DELETE CASCADE,
	CONSTRAINT fk_card
		FOREIGN KEY(card_id)
			REFERENCES cards(card_id)
			ON DELETE CASCADE
);
