CREATE TABLE cards (
  card_id bigserial PRIMARY KEY,
  user_id bigint,
  title VARCHAR ( 255 ) UNIQUE NOT NULL,
  remark VARCHAR ( 255 ),
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP,
  CONSTRAINT fk_user
  	FOREIGN KEY(user_id)
  		REFERENCES users(user_id)
  		ON DELETE CASCADE
);