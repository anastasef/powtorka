ALTER SEQUENCE users_user_id_seq AS bigint;
ALTER TABLE users ALTER COLUMN user_id SET DATA TYPE bigint;