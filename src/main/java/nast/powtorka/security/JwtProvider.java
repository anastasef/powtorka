package nast.powtorka.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;
import lombok.extern.slf4j.Slf4j;
import nast.powtorka.exception.PowtorkaException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;

@Slf4j
@Service
public class JwtProvider {

    private KeyStore keyStore;

    private final String KEYSTORE_PATH;
    private final String KEYSTORE_PASS;
    private final String KEYS_ALIAS;

    public JwtProvider(
            @Value("${app.keystore.path}") String keystorePath,
            @Value("${app.keystore.pass}") String keystorePass,
            @Value("${app.keystore.alias}") String keysAlias) {
        this.KEYSTORE_PATH = keystorePath;
        this.KEYSTORE_PASS = keystorePass;
        KEYS_ALIAS = keysAlias;
    }

    /**
     * Load the keystore from a .JKS file
     */
    @PostConstruct
    public void init() {

        try {
            keyStore = KeyStore.getInstance("JKS");
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        // KeyStore.load() resets the key store to the empty state
        // when passed a null input stream,
        // so we need to be sure that the file was found.

        InputStream keystoreAsStream = getClass().getResourceAsStream(KEYSTORE_PATH);

        if (keystoreAsStream == null) throw new PowtorkaException("keystore file was not found");

        try {
            keyStore.load(keystoreAsStream, KEYSTORE_PASS.toCharArray());
        } catch (NoSuchAlgorithmException | CertificateException | IOException e) {
            throw new PowtorkaException("Exception occured while loading keystore: " + e.getMessage());
        }
    }

    public String generateToken(Authentication authentication) {
        User principal = (User) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject(principal.getUsername())
                .signWith(getPrivateKey())
                .compact();
    }

    public boolean validateToken(String jwt) {
        try {
            Jwts.parserBuilder()
                    .setSigningKey(getPublicKey())
                    .build()
                    .parseClaimsJws(jwt);

            return true;
        } catch (SignatureException e) {
            log.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        }

        return false;
    }

    public String getUsernameFromJWT(String jwt) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(getPublicKey())
                .build()
                .parseClaimsJws(jwt)
                .getBody();
        return claims.getSubject();
    }

    private PrivateKey getPrivateKey() {
        try {
            if (!keyStore.containsAlias(KEYS_ALIAS))
                throw new PowtorkaException("KeyStore does not contain the alias \"" + KEYS_ALIAS + "\"");

            return (PrivateKey) keyStore.getKey(KEYS_ALIAS, KEYSTORE_PASS.toCharArray());
        } catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
            throw new PowtorkaException("Exception occured while retrieving private key from keystore");
        }
    }

    private PublicKey getPublicKey() {
        try {
            return keyStore.getCertificate(KEYS_ALIAS).getPublicKey();
        } catch (KeyStoreException e) {
            throw new PowtorkaException("Exception occurred while retrieving public key from keystore");
        }
    }

}
