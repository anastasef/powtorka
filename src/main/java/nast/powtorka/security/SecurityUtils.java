package nast.powtorka.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public final class SecurityUtils {
	
    /**
	 * Get current authenticated user
	 * @return current authenticated user
	 * @throws BadCredentialsException if user is not authorized
	 */
	public static String getAuthenticatedUserUsername() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username;
		
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			username = auth.getName();
		} else {
			throw new BadCredentialsException("You should be logged in");
		}
		
		return username;
	}
}
