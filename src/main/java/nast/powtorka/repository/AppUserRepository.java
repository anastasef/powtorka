package nast.powtorka.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import nast.powtorka.model.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

	Optional<AppUser> findByUsername(String username);
	
}
