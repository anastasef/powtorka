package nast.powtorka.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import nast.powtorka.model.Revision;

public interface RevisionRepository extends JpaRepository<Revision, Long> {

}
