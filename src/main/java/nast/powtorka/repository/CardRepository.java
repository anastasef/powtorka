package nast.powtorka.repository;

import java.util.Optional;

//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import nast.powtorka.model.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, Long>{

	@Query("SELECT c FROM Card c " +
			" WHERE LOWER(c.title) = LOWER(:searchTitle)")
	Optional<Card> findByTitleIgnoreCase(@Param("searchTitle") String title);
	
//	@Query("SELECT c FROM Card c")
//	public Page<Card> findAllPage(Pageable pageable);
}
