package nast.powtorka.model;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "revisions")
@NoArgsConstructor
public class Revision {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqRevisionGen")
    @GenericGenerator(
            name = "seqRevisionGen",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "revisions_revision_id_seq"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @Column(name = "revision_id")
    @Setter(value = AccessLevel.NONE)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private AppUser revisionOwner;

    // card_id is unique
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "card_id", unique = true)
    private Card card;

    @Column(name = "times_revised", nullable = false)
    private int timesRevised;

    @Column(name = "active", nullable = false)
    private boolean isActive;

    @Column(name = "next_revision")
    private LocalDateTime nextRevision;

    @Column(name = "last_revision")
    private LocalDateTime lastRevision;

    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;


    public Revision(@NotNull Card card) {
        this.revisionOwner = card.getCardOwner();
        this.card = card;
        this.createdAt = LocalDateTime.now();
        this.timesRevised = 0;
        this.isActive = true;
        this.lastRevision = null;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (this == other) return true;
        if (!(other instanceof Revision)) return false;

        Revision otherRev = (Revision) other;

        return Objects.equals(this.id, otherRev.id);
    }

    @Override
    public int hashCode() {
        int res = id.intValue();
        res = 31 * res + createdAt.hashCode();
        res = 31 * res + nextRevision.hashCode();

        return res;
    }


}
