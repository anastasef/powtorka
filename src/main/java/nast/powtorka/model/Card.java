package nast.powtorka.model;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "cards")
@NoArgsConstructor
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCardGen")
    @GenericGenerator(
            name = "seqCardGen",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "cards_card_id_seq"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @Column(name = "card_id")
    private Long id;

    @Column(unique = true)
    @NotBlank
    @Size(max = 255)
    private String title;

    @Column(unique = true)
    @Size(max = 255)
    private String remark;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private AppUser cardOwner;

    @Column(name = "created_at", nullable = false, updatable = false) // used to be insertable = false
    private LocalDateTime createdAt;

    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    public Card(@NotBlank @Size(max = 255) String title, @Size(max = 255) String remark) {
        this.title = title;
        this.remark = remark;
        this.createdAt = LocalDateTime.now();
        this.updatedAt = null;
    }

    public void update(String title, String remark) {
        this.title = title;
        this.remark = remark;
        this.updatedAt = LocalDateTime.now();
    }

    @Override
    public int hashCode() {
        int res = id.intValue();
        res = 31 * res + title.hashCode();
        res = 31 * res + (remark != null ? remark.hashCode() : 0);

        return res;
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) return false;
        if (this == other) return true;
        if (!(other instanceof Card)) return false;

        Card otherCard = (Card) other;

        return Objects.equals(this.id, otherCard.id) && this.title.equals(otherCard.getTitle());
    }


}
