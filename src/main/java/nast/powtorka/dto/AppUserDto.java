package nast.powtorka.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppUserDto {
	private String username;
}
