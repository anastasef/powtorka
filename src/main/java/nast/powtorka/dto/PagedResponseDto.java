package nast.powtorka.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
@ApiModel(description = "Paged response DTO")
public abstract class PagedResponseDto<T> {

    @ApiModelProperty()
    protected long count;

    @ApiModelProperty(position = 1)
    protected int currentPage;

    @ApiModelProperty(position = 2)
    protected int totalPages;

    @ApiModelProperty(position = 3)
    protected Iterable<T> data;
}
