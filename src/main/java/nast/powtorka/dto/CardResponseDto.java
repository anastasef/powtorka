package nast.powtorka.dto;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "Card response DTO")
public class CardResponseDto {
	
	@ApiModelProperty(position = 1)
	private Long id;
	
	@ApiModelProperty(position = 2, notes = "Unique case insensitive title", example = "Sample title")
	private String title;
	
	@ApiModelProperty(position = 3, notes = "Some notes about this card", example = "Some notes about this card")
	private String remark;
	
	@ApiModelProperty(position = 4)
	private LocalDateTime createdAt;
	
	@ApiModelProperty(position = 5, notes = "NULL if card was not modified after creation")
	private LocalDateTime updatedAt;
}
