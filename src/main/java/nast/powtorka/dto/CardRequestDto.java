package nast.powtorka.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import nast.powtorka.exception.validator.UniqueCardTitle;

@Getter
@Setter
@ApiModel(description = "Create card request DTO")
public class CardRequestDto {
	
	@UniqueCardTitle
	@Size(min = 1, max = 255, message = "Title should be 1 to 255 characters long")
	@NotBlank(message = "Please provide a title")
	@ApiModelProperty(position = 1, required = true, example = "Sample title")
	private String title;
	
	@ApiModelProperty(position = 2, required = false, example = "Some notes about this card")
	private String remark;
}
