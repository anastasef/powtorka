package nast.powtorka.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "Login request")
public class LoginRequestDto {
	
	@Size(min = 4, max = 50, message = "Username incorrect length")
	@NotEmpty(message = "Please provide a username")
	@ApiModelProperty(position = 1, required = true, example = "Pupa")
	private String username;
	
	@Size(min = 8, max = 22, message = "Password incorrect length")
	@NotEmpty(message = "Please provide a password")
	@ApiModelProperty(position = 2, required = true, example = "pupapass")
	private String password;
}
