package nast.powtorka.dto;

import nast.powtorka.model.Card;
import org.springframework.data.domain.Page;

public class PagedCardResponseDto extends PagedResponseDto<Card> {

    public PagedCardResponseDto(Page<Card> page) {
        this.count = page.getTotalElements();
        this.currentPage = page.getNumber();
        this.totalPages = page.getTotalPages();
        this.data = page.getContent();
    }
}
