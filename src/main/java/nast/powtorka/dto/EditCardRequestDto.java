package nast.powtorka.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
@ApiModel(description = "Edit card request DTO")
public class EditCardRequestDto {
	
	@Size(min = 1, max = 255, message = "Title should be 1 to 255 characters long")
	@NotBlank(message = "Please provide a title")
	@ApiModelProperty(position = 2, required = true, example = "Sample title")
	private String title;
	
	@ApiModelProperty(position = 3, required = true, example = "Some notes about this card")
	private String remark;
}
