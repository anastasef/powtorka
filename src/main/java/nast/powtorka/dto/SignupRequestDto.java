package nast.powtorka.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import nast.powtorka.exception.validator.UniqueUsername;

@Getter
@AllArgsConstructor
public class SignupRequestDto {

    @UniqueUsername
    @Size(min = 4, max = 50, message = "Username length should be at least 4 characters long")
    @NotBlank(message = "Please provide a username")
    @ApiModelProperty(notes = "Unique username. Case sensitive", position = 1, required = true, example = "Pupa")
    private String username;

    @Size(min = 8, max = 22, message = "Password length should be 8 to 22 characters long")
    @NotEmpty(message = "Please provide a password")
    @ApiModelProperty(position = 2, required = true, example = "pupapass")
    private String password;
}
