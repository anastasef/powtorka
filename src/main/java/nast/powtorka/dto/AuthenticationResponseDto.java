package nast.powtorka.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@ApiModel
public class AuthenticationResponseDto {
	@ApiModelProperty(name = "Authentication token", notes = "JWT")
	private String authToken;
}
