package nast.powtorka.exception;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class EntityNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 6676662821016314231L;

    /**
     * Custom Entity Not found Exception.<br>
     * Returns an exception with a message describing
     * which entity was not found for which search parameters.
     *
     * @param clazz           a class of a model
     * @param searchParamsMap search params as strings, separated by ",".<br>
     *                        Example: <code>"key1", "val1", "key2", "val2"</code>
     */
    public EntityNotFoundException(Class clazz, String... searchParamsMap) {
        super(EntityNotFoundException.generateMessage(
                clazz.getSimpleName(),
                toMap(String.class, String.class, searchParamsMap)));
    }

    private static String generateMessage(String entity, Map<String, String> searchParams) {
        // TODO Maybe later replace with Apache StringUtils
        String capEntity = entity.substring(0, 1).toUpperCase() + entity.substring(1);

        return capEntity + " was not found for parameters " + searchParams;
    }

    private static <K, V> Map<K, V> toMap(Class<K> keyType, Class<V> valueType, Object... entries) {
        // TODO rethink the generalization
        if (entries.length % 2 == 1) throw new IllegalArgumentException("Invalid entries");

        return IntStream.range(0, entries.length / 2).map(i -> i * 2)
                .collect(HashMap::new,
                        (m, i) -> m.put(keyType.cast(entries[i]), valueType.cast(entries[i + 1])),
                        Map::putAll);
    }


}
