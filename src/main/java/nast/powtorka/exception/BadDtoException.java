package nast.powtorka.exception;

public class BadDtoException extends RuntimeException {

    private static final long serialVersionUID = 834785149737898068L;

    public BadDtoException(String message) {
        super(message);
    }
}
