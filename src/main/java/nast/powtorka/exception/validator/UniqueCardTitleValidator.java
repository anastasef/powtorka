package nast.powtorka.exception.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import nast.powtorka.service.CardService;

public class UniqueCardTitleValidator implements ConstraintValidator<UniqueCardTitle, String> {

    @Autowired
    CardService cardService;

    @Override
    public boolean isValid(String title, ConstraintValidatorContext context) {
        return title != null && !cardService.isTitleAlreadyInUse(title);
    }

}
