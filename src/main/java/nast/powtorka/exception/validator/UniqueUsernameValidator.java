package nast.powtorka.exception.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import nast.powtorka.service.AppUserService;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

    @Autowired
    AppUserService userService;

    @Override
    public boolean isValid(String username, ConstraintValidatorContext context) {
        return username != null && !userService.isUsernameAlreadyInUse(username);
    }
}
