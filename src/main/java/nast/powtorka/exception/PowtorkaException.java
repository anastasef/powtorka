package nast.powtorka.exception;

public class PowtorkaException extends RuntimeException {

	private static final long serialVersionUID = -4077584201519284316L;

	public PowtorkaException(String message) {
		super(message);
	}

}
