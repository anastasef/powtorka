package nast.powtorka.api;

import java.net.URI;

import javax.validation.Valid;

import nast.powtorka.dto.PagedResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nast.powtorka.dto.CardRequestDto;
import nast.powtorka.dto.CardResponseDto;
import nast.powtorka.dto.EditCardRequestDto;
import nast.powtorka.service.CardService;

@RestController
@RequestMapping("api/v1/cards")
public class CardController {
	
	@Autowired
	private CardService cardService;
	
	@GetMapping(path = "{id}")
	@ApiOperation(
			value = "Get card by id",
			response = CardResponseDto.class
	)
	@ApiResponses({
		@ApiResponse(code = 404, message = "Card not found"),
		@ApiResponse(code = 403, message = "User should be authenticated | User can get only his own cards")
	})
	public ResponseEntity<?> getCardById(@ApiParam(value = "card id", required = true) @PathVariable Long id) {
		return new ResponseEntity(cardService.getCardById(id), HttpStatus.OK);
	}
	
	@GetMapping
	@ApiOperation(
			value = "Get a page of cards",
			response = PagedResponseDto.class
	)
	public ResponseEntity<?> getCardsPage(
			@RequestParam(value = "page", required = false, defaultValue = "0") int pageNum,
			@RequestParam(value = "size", required = false, defaultValue = "30") int pageSize,
			@RequestParam(value = "sort", required = false, defaultValue = "id") String sortBy
	) {
		return new ResponseEntity(cardService.getCardsPage(pageNum, pageSize, sortBy), HttpStatus.OK);
	}
	
	@PostMapping
	@ApiOperation(value = "Create a card; card's revision schedule is generated automatically",
				notes = "Card's title should be unique (case insensitive)",
				response = CardResponseDto.class)
	@ApiResponses({
		@ApiResponse(code = 400, message = "Validation errors | Card title not unique (case insensitive)"),
		@ApiResponse(code = 403, message = "User should be authenticated")
	})
	public ResponseEntity<?> createCard(@Valid @RequestBody CardRequestDto cardDTO) {

		CardResponseDto cardRespDTO = cardService.createCard(cardDTO);
		
		URI path = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(cardRespDTO.getId())
				.toUri();
		return ResponseEntity.created(path).body(cardRespDTO);
	}
	
	@PutMapping(path = "{id}")
	@ApiOperation(value = "Create a card",
		notes = "Card's title should be unique (case insensitive)",
		response = CardResponseDto.class)
	@ApiResponses({
		@ApiResponse(code = 404, message = "Card with this id does not exist"),
		@ApiResponse(code = 400, message = "Validation errors | Card with this title already exists"),
		@ApiResponse(code = 403, message = "User should be authenticated | User can't edit other users' cards")
	})
	public ResponseEntity<?> editCard(
			@ApiParam(value = "card id", required = true) @PathVariable Long id, 
			@Valid @RequestBody EditCardRequestDto cardDTO) {
		CardResponseDto cardRespDTO = cardService.editCard(id, cardDTO);
		URI path = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(cardRespDTO.getId())
				.toUri(); // TODO finish expand path
		return new ResponseEntity(cardRespDTO, HttpStatus.OK);
	}

}