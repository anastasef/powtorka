package nast.powtorka.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import nast.powtorka.dto.AppUserDto;
import nast.powtorka.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class AppUserController {
	
	@Autowired
	private AppUserService userService;

	@GetMapping(path = "{id}")
	@ApiOperation(
			value = "Get user by id",
			response = AppUserDto.class
	)
	public ResponseEntity<?> getUserById(@ApiParam(value = "user id", required = true) @PathVariable Long id) {
		return new ResponseEntity(userService.getUserById(id), HttpStatus.OK);
	}
}
