package nast.powtorka.api;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nast.powtorka.dto.AuthenticationResponseDto;
import nast.powtorka.dto.LoginRequestDto;
import nast.powtorka.dto.SignupRequestDto;
import nast.powtorka.service.AuthService;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

	@Autowired
	private AuthService authService;

	@PostMapping("/signup")
	@ApiResponses({
		@ApiResponse(code = 400, message = "Validation error: username already in use"),
		@ApiResponse(code = 201, message = "Created")
	})
	@ResponseStatus(HttpStatus.CREATED)
	public void signup(@Valid @RequestBody SignupRequestDto reqDTO) {
		authService.signup(reqDTO);
	}

	@PostMapping("/login")
	@ApiOperation(value = "Log in as an existing user",
			notes = "Provide a username and a password. Method returns a bearer token.",
			response = AuthenticationResponseDto.class)
	@ApiResponses({
		@ApiResponse(code = 400, message = "Validation errors"),
		@ApiResponse(code = 401, message = "Bad credentials")
	})
	@ResponseStatus(HttpStatus.OK)
	public AuthenticationResponseDto login(@Valid @RequestBody LoginRequestDto reqDTO) {
		return authService.login(reqDTO);
	}
	
}
