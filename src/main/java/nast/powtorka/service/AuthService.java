package nast.powtorka.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import nast.powtorka.dto.AuthenticationResponseDto;
import nast.powtorka.dto.LoginRequestDto;
import nast.powtorka.dto.SignupRequestDto;
import nast.powtorka.model.AppUser;
import nast.powtorka.repository.AppUserRepository;
import nast.powtorka.security.JwtProvider;

@Service
public class AuthService {

    @Autowired
    private AppUserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtProvider jwtProvider;

    public void signup(SignupRequestDto signupRequestDTO) {

        AppUser user = new AppUser();
        user.setUsername(signupRequestDTO.getUsername());
        user.setPassword(passwordEncoder.encode(signupRequestDTO.getPassword()));

        userRepository.save(user);
    }

    public AuthenticationResponseDto login(LoginRequestDto loginRequestDTO) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequestDTO.getUsername(),
                        loginRequestDTO.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String authToken = jwtProvider.generateToken(authentication);

        return new AuthenticationResponseDto(authToken);
    }

    public Optional<User> getCurrentUser() {
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return Optional.of(principal);
    }
}
