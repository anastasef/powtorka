package nast.powtorka.service;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import nast.powtorka.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import nast.powtorka.exception.BadDtoException;
import nast.powtorka.exception.EntityNotFoundException;
import nast.powtorka.exception.PowtorkaException;
import nast.powtorka.model.AppUser;
import nast.powtorka.model.Card;
import nast.powtorka.repository.AppUserRepository;
import nast.powtorka.repository.CardRepository;
import nast.powtorka.security.SecurityUtils;

@Service
public class CardService {
	
	@Autowired
	private RevisionService revisionService;
	
	@Autowired
	private CardRepository cardRepository;
	
	@Autowired
	private AppUserRepository userRepository;
	
	public CardResponseDto getCardById(Long id) {
		Card card = cardRepository.findById(id)
				.orElseThrow(()  -> new EntityNotFoundException(Card.class, "id", id.toString()));
		
		if (!isUserCardOwner(card)) throw new AccessDeniedException("You are not allowed to view cards of other users");
		
		return mapFromCardToRespDTO(card);
	}
	
	public PagedResponseDto getCardsPage(int pageNum, int pageSize, String sortBy) {
		Pageable pageable = PageRequest.of(pageNum, pageSize, Sort.by(sortBy));
		Page<Card> page = cardRepository.findAll(pageable);

		return new PagedCardResponseDto(page);
	}
	
	/**
	 * Alongside with card, a card's revision is created.
	 * @param cardDTO
	 * @return
	 */
	@Transactional
	public CardResponseDto createCard(CardRequestDto cardDTO) {
		
		AppUser user = loadCurrentUser();
		
		Card card = new Card(cardDTO.getTitle(), cardDTO.getRemark());
		card.setCardOwner(user);
		
		if (user.getCards() == null) {
			user.setCards(new ArrayList<>());
		}
		user.getCards().add(card);

		cardRepository.save(card);
		revisionService.createRevision(card);
		
		return mapFromCardToRespDTO(card);
	}
	
	@Transactional
	public CardResponseDto editCard(Long cardId, EditCardRequestDto cardDTO) {
		
		// DTO валидное
		
		// Выбираем по id карточку из бд
		// Обновляем ей поля 
		
		// Если в БД нет карточки с таким id, то исключение 404
		// Если карточка не принадлежит текущему юзеру, то исключение 403
		// Если в бд есть другая карточка с тем же заголовком, то исключение 400
		
		
		AppUser user = loadCurrentUser();
		
		Card card = cardRepository.findById(cardId)
				.orElseThrow(()  -> new EntityNotFoundException(Card.class, "id", cardId.toString()));
		
		if (!isUserCardOwner(card)) throw new AccessDeniedException("You are not allowed to edit cards of other users");
		
		Optional<Card> cardPossibleDuplicate = cardRepository.findByTitleIgnoreCase(cardDTO.getTitle());
		
		if (cardPossibleDuplicate.isPresent() && !cardPossibleDuplicate.get().getId().equals(cardId)) {
			throw new BadDtoException("Card with this title already exists");
		}
		
		card.update(cardDTO.getTitle(), cardDTO.getRemark());
		cardRepository.save(card);
		
		return mapFromCardToRespDTO(card);
	}
	
	private boolean isUserCardOwner(Card card) {
		
		AppUser user = loadCurrentUser();
		
		return card.getCardOwner().getId() == user.getId();
	}
	
	// Used in nast.powtorka.exception.validator.UiqueCardTitleValidator
	public boolean isTitleAlreadyInUse(String title) {
		return (cardRepository.findByTitleIgnoreCase(title).isEmpty()) ? false : true;
	}
	
	private AppUser loadCurrentUser() {
		return userRepository.findByUsername(SecurityUtils.getAuthenticatedUserUsername())
				.orElseThrow(() -> new PowtorkaException("Current user was not found"));
	}
	
	private CardResponseDto mapFromCardToRespDTO(Card card) {
		CardResponseDto cardDTO = new CardResponseDto();
		cardDTO.setId(card.getId());
		cardDTO.setTitle(card.getTitle());
		cardDTO.setRemark(card.getRemark());
		cardDTO.setCreatedAt(card.getCreatedAt());
		cardDTO.setUpdatedAt(card.getUpdatedAt());

		return cardDTO;
	}
}
