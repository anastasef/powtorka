package nast.powtorka.service;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import nast.powtorka.model.AppUser;
import nast.powtorka.model.Card;
import nast.powtorka.model.Revision;
import nast.powtorka.repository.RevisionRepository;

@Service
public class RevisionService {

    private RevisionRepository revRepo;

    /**
     * Offsets in days for the next repetition.
     * Index of the array is current day, value is the offset for the next repetition.
     * Example:
     * Today is the 4th day of revision, so the next revision will be on
     * today + REVISION_SCHEDULE[4].revisionService
     */
    private final int[] REVISION_SCHEDULE = {1, 3, 5, 8, 14, 21, 30, 44, 60, 90, 120, 150, 210, 300, 365, 455};


    private LocalDateTime calcNextRevision(LocalDateTime lastRevision, int timesRevised) {
        return lastRevision.plusDays(REVISION_SCHEDULE[timesRevised]);
    }


    public void createRevision(Card card) {
        AppUser cardOwner = card.getCardOwner();

        Revision revision = new Revision(card);
        revision.setNextRevision(calcNextRevision(LocalDateTime.now(), 0));

        if (cardOwner.getRevisions() == null) {
            cardOwner.setRevisions(new ArrayList());
        }
        cardOwner.getRevisions().add(revision);
    }
}
