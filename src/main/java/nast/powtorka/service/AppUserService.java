package nast.powtorka.service;

import nast.powtorka.dto.AppUserDto;
import nast.powtorka.exception.EntityNotFoundException;
import nast.powtorka.model.AppUser;
import nast.powtorka.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppUserService {

    @Autowired
    private AppUserRepository userRepository;

    public AppUserDto getUserById(Long id) {

        AppUser user = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(AppUser.class, "id", id.toString()));
        return mapFromUserToDTO(user);
    }

    public boolean isUsernameAlreadyInUse(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    private AppUserDto mapFromUserToDTO(AppUser user) {
        AppUserDto userDTO = new AppUserDto();
        userDTO.setUsername(user.getUsername());

        return userDTO;
    }
}
